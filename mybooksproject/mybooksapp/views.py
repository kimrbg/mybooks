from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from .models import BookList, BookGenre, Book

def index(request):
    listen = BookList.objects.all()
    return render(request, 'mybooksapp/index.html', {'listen':listen})

def detail_list(request, booklist_id):
    liste = get_object_or_404(BookList, pk=booklist_id)
    genres = BookGenre.objects.all()
    return render(request, 'mybooksapp/detail_list.html', {'liste':liste, 'genres':genres})

def detail_genre(request, bookgenre_id):
    genre = get_object_or_404(BookGenre, pk=bookgenre_id)
    books = Book.objects.filter(genre=bookgenre_id)
    return render(request, 'mybooksapp/detail_genre.html', {'genre':genre, 'books':books})

def detail_book(request, book_id):
    book = get_object_or_404(Book, pk=book_id)
    return render(request, 'mybooksapp/detail_book.html', {'book':book})

def new_book(request):
    if request.method == 'POST':
        title = request.POST["title"]
        author = request.POST["author"]
        content = request.POST["content"]
        rating = request.POST["rating"]
        price = request.POST["price"]
        cover = request.POST["cover"]
        booklist = BookList.objects.get(pk=request.POST["booklist"])
        genre = BookGenre.objects.get(pk=request.POST["genre"])
        Book.objects.create(title=title,
                            author=author,
                            content=content,
                            rating=rating,
                            price=price,
                            cover=cover,
                            booklist=booklist,
                            genre=genre,)
        return HttpResponseRedirect(reverse('mybooksapp:detail_list', args=[booklist.id]))

    return render(request, 'mybooksapp/new_book.html', {"booklists": BookList.objects.all(), "genres": BookGenre.objects.all()})

def delete_book(request, book_id):
    book = get_object_or_404(Book, pk=book_id)
    book.delete()
    return HttpResponseRedirect(reverse('mybooksapp:index'))

def update_book_def(book, post):
    if "book_title" in post:
        book.title = post["book_title"]
    if "book_author" in post:
        book.author = post["book_author"]
    if "book_content" in post:
        book.content = post["book_content"]    
    if "book_rating" in post:
        try:
            rating = int(post["book_rating"]) 
        except ValueError:
            rating = 0
        book.rating = rating
    book.done = "book_done" in post
    if "book_price" in post:
        try:
            price = float(post["book_price"]) 
        except ValueError:
            price = 0
        book.price = price
    if "book_cover" in post:
        book.cover = post["book_cover"]
    if "book_booklist" in post:
        book.booklist = get_object_or_404(BookList, pk=post["book_booklist"])
    if "book_genre" in post:
        book.genre = get_object_or_404(BookGenre, pk=post["book_genre"])
    book.save()


def update_book(request, book_id):
    book = get_object_or_404(Book, pk=book_id)
    if request.POST:
        update_book_def(book, request.POST)
        return HttpResponseRedirect(reverse('mybooksapp:detail_book', args=[book.id]))
    listen = BookList.objects.all()
    genres = BookGenre.objects.all()
    context = {
            "listen": listen, 
            "genres": genres, 
            "book": book, 
            }
    return render(request, "mybooksapp/update_book_def.html", context)

